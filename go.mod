module github.com/jfcote87/esign

go 1.20

require (
	github.com/jfcote87/ctxclient v0.6.2
	github.com/jfcote87/oauth2 v0.4.0
	github.com/jfcote87/testutils v0.1.0
)
