# github.com/jfcote87/ctxclient v0.6.2
## explicit; go 1.16
github.com/jfcote87/ctxclient
# github.com/jfcote87/oauth2 v0.4.0
## explicit; go 1.13
github.com/jfcote87/oauth2
github.com/jfcote87/oauth2/jws
github.com/jfcote87/oauth2/jwt
# github.com/jfcote87/testutils v0.1.0
## explicit; go 1.11
github.com/jfcote87/testutils
